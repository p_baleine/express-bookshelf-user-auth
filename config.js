module.exports = {
  "directory": "./db/migrations",
  "database": {
    "client": "sqlite3",
    "connection": {
      "filename": "./db/blog-dev.db"
    },
    "debug": true
  }
};
