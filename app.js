
/**
 * Module dependencies.
 */

var express = require('express')
  , _ = require('underscore') // ← 追加
  , posts = require('./routes/posts') // ← 追加
  , admin = require('./admin') // ← 追加
  , http = require('http')
  , path = require('path')
  , Resource = require('express-resource'); // ← 追加

var app = module.exports = express(); // ← 変更

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser('your secret here'));
  app.use(express.session());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

// ↓変更

app.get('/', posts.index);

app.resource('posts', _.pick(posts, 'index', 'show')); // 未ログインだと`index`と`show`のみアクセス可

// adminサブアプリをマウント
app.use('/admin', admin);

if (!module.parent) {
  app.listen(app.get('port'));
  console.log("Express server listening on port " + app.get('port'));
}
