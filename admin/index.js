
/**
 * Module dependencies.
 */

var express = require('express')
  , posts = require('../routes/posts')
  , sessions = require('../routes/sessions')
  , http = require('http')
  , path = require('path')
  , Resource = require('express-resource')
  , User = require('../models/user');

var app = module.exports = express();

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/../views');
  app.set('view engine', 'jade');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser('your secret here'));
  app.use(express.session());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

app.all(/^\/posts/, restrict, loadUser);

app.resource('posts', restrict, posts);
app.resource('sessions', sessions);

function restrict(req, res, next) {
  if (!req.session.uid) { return res.redirect('/'); }
  next();
}

function loadUser(req, res, next) {
  if (!req.session.uid) { return next(); }
  new User({ id: req.session.uid }).fetch().then(function(user) {
    req.user = user;
    next();
  });
}
