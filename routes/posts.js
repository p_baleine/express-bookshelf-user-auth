exports.index = function(req, res) {
  res.send('投稿一覧、' + (req.user ? 'ログイン済' : '未ログイン'));
};

exports.show = function(req, res) {
  res.send('投稿 ' + req.params.post + 'の詳細');
};

exports.new = function(req, res) {
  res.send('新規投稿作成画面');
};

exports.cretate = function(req, res) {
  res.send('新規投稿作成');
};

exports.edit = function(req, res) {
  res.send('投稿 ' + req.params.post + 'の編集画面');
};

exports.update = function(req, res) {
  res.send('投稿 ' + req.params.post + 'の更新');
};

exports.del = function(req, res) {
  res.send('投稿 ' + req.params.post + 'の削除');
};
