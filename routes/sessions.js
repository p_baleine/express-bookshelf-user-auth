var User = require('../models/user');

exports.new = function(req, res) {
  res.render('session/new');
};

exports.create = function(req, res) {
  var user = req.body.user;

  User.authenticate(user.email, user.password).then(function(user) {
    req.session.uid = user.id;
    res.redirect('/admin/posts');
  }, function(e) {
    req.session.uid = null;
    res.statusCode = 404;
    res.render('session/new', { error: e.message });
  });
};
