var Promise = require('bluebird'),
    bcrypt = Promise.promisifyAll(require('bcrypt')),
    Bookshelf = require('bookshelf'),

    // database setting
    config = require('../config'),

    // initialize database
    blogBookshelf = Bookshelf.blogBookshelf = Bookshelf.initialize(config.database);

/**
 * User model
 */

var User = module.exports = blogBookshelf.Model.extend({

  tableName: 'users',

  hasTimestamps: true,

  initialize: function() {
    blogBookshelf.Model.prototype.initialize.apply(this, arguments);
    this.on('saving', this.hashPassword, this);
  },

  /**
   * Set salted password.
   */

  hashPassword: function() {
    var _this = this;

    return bcrypt.genSaltAsync(10)
      .then(function(salt) {
        _this.set('salt', salt);
        return bcrypt.hashAsync(_this.get('password'), salt);
      })
      .then(function(hash) {
        return _this.set('password', hash);
      });
  }

}, {

  /**
   * Authenticate user.
   *
   * @param {String} email
   * @param {String} password
   * @return {Promise}
   */

  authenticate: function(email, password) {
    return new this({ email: email }).fetch({ require: true })
      .then(function(user) {
        return [bcrypt.hashAsync(password, user.get('salt')), user];
      })
      .spread(function(hash, user) {
        if (hash === user.get('password')) { return user; }
        throw new Error('password or email is incorrect.');
      });
  }

});
